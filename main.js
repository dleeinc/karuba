$(document).ready(function(){

	var nums = [];

	for (var i = 1; i <= 36; i++) {
	    nums.push(i);
	}

	nums.sort(function(a, b){return 0.5 - Math.random()});

	i = 0;

	$('#reset_button').hide();
	$('#score_button').hide();

    $('#output').text(nums[0]);

    $('#next_button').click(function(){
    	$('#output').text(nextItem());
    });

    $('#reset_button').click(function(){
    	$('#output').text(reset());
    });

    // FUNCTIONS

    function nextItem() {
    	ga('send', 'event', 'click', 'next');
        i = i + 1; // increase i by one

        if (i % nums.length == 0) {
        	$('#output').text("Game Over");
        	$('#next_button').hide();
        	$('#reset_button').show();
        } else {

        }

        return nums[i]; // give us back the item of where we are now
    }

    function prevItem() {
        if (i === 0) { // i would become 0
            i = nums.length; // so put it at the other end of the numsay
        }
        i = i - 1; // decrease by one
        return nums[i]; // give us back the item of where we are now
    }

    function reset() {
    	ga('send', 'event', 'click', 'reset');
    	$('#reset_button').hide();
    	$('#next_button').show();

    	i = 0;
    	return nums[i];
    }

})

// document.getElementById('prev_button').addEventListener(
//     'click', // we want to listen for a click
//     function (e) { // the e here is the event itself
//         document.getElementById('output').textContent = prevItem();
//     }
// );